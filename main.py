from flask import Flask, render_template, request, session, abort
import random
import string
import os
import socket
app = Flask(__name__)

@app.route("/")
@app.route("/<name>")
def hello(name=None):
    #return "Hello World!"
	return render_template("hello.html", name=name)
@app.route("/ping", methods=['GET', 'POST'])
#@app.route("/ping/<hostname>")
def check():
	'''
	response = os.system("ping -c 2 "+ hostname + "> /dev/null 2>&1")
	if response == 0:
		return "<font color='green'>%s is up!</font>" % hostname
	else:
		return "<font color='red'>%s is down!</font>" % hostname
	'''
	if request.method == "POST":
		if request.form.get("hostname") == "" and request.form.get("port") == "":
			return "Please fill up form"
		elif request.form.get("hostname") != "" and request.form.get("port") == "":
			response = os.system("ping -c 2 "+ request.form.get("hostname") +"> /dev/null 2>&1")
			if response == 0:
				return "<font color='green'>%s is up!</font>" % request.form.get("hostname")
			else:
				return "<font color='red'>%s is down!</font>" % request.form.get("hostname")
		elif request.form.get("hostname") != "" and request.form.get("port") != "":
			s = socket.socket()
			s.settimeout(5)
			try:
				s.connect((request.form.get("hostname"), int(request.form.get("port"))))
				return "<font color='green'>Connected to %s on port %s" % (request.form.get("hostname"), request.form.get("port"))
			except socket.error:
				return "<font color='red'>Connected to %s on port %s failed" % (request.form.get("hostname"), request.form.get("port"))
		else:
			return "Something is wrong"
	else:
		return render_template("index.html")

@app.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.pop('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            abort(403)

def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = some_random_string()
    return session['_csrf_token']

app.jinja_env.globals['csrf_token'] = generate_csrf_token
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'


def some_random_string():
    return ''.join(random.choice(string.ascii_letters) for x in range(13))

if __name__ == '__main__':
    #app.run()
    app.run(port=5000, debug=True)
